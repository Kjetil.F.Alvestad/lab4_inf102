package INF102.lab4.sorting;

import java.util.List;

public class BubbleSort implements ISort {

  @Override
  public <T extends Comparable<T>> void sort(List<T> list) { // O(n^2)
    boolean swapMade = false;

    for (int i = 0; i < list.size(); i++) { // O(n)
      for (int j = 0; j < list.size() - 1; j++) { // O(n)
        T current = list.get(j); // O(1)
        T next = list.get(j + 1); // O(1)

        if (current.compareTo(next) > 0) { //O(1)
          list.set(j, next); // O(1)
          list.set(j + 1, current); // O(1)
          swapMade = true;
        }
      }
      if (!swapMade) {
        break;
      }

      swapMade = false;
    }
  }
}
