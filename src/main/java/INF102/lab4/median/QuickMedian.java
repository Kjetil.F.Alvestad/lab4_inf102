package INF102.lab4.median;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class QuickMedian implements IMedian {

  @Override
  public <T extends Comparable<T>> T median(List<T> list) {
    List<T> listCopy = new ArrayList<>(list); // Method should not alter list
    int k = (listCopy.size() / 2) + 1;
    return pickKth(listCopy, k);
  }

  private <T extends Comparable<T>> T pickKth(List<T> list, int k) {
    if (list.size() == 1) {
      return list.get(0);
    }

    Random rnd = new Random();
    int index = rnd.nextInt(list.size());
    T pivot = list.get(index);

    List<T> lo = new ArrayList<>();
    List<T> hi = new ArrayList<>();

    for (int i = 0; i < list.size(); i++) {
      if (i == index) {
        continue;
      }
      int value = list.get(i).compareTo(pivot);
      if (value == 0 || value < 0) {
        lo.add(list.get(i));
      } else if (value > 0) {
        hi.add(list.get(i));
      }
    }

    if ((lo.size()) + 1 == k) {
      return pivot;
    } else if (lo.size() >= k) {
      return pickKth(lo, k);
    } else {
      return pickKth(hi, k - (lo.size() + 1));
    }
  }
}
